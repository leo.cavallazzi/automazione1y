#include <stdio.h>
#define STOP 0
#define NMAX 50

void acquisizione_controllata (int [], int*);
void calcolaMinori (int [], int [], int);

int main(int argc, char * argv[])
{
	int seq[NMAX], seq_ris[NMAX], dim, i;

	acquisizione_controllata (seq, &dim);

	calcolaMinori (seq, seq_ris, dim);

	for (i = 0; i < dim; i++) {
		printf("%d ", seq_ris[i]);
	}
	printf("\n");

	return 0;
}

void acquisizione_controllata (int seq[], int*dim)
{
	int val, i;
	
	scanf("%d", &val);
	for (i = 0; val > STOP && i < NMAX; i++) {
		seq[i] = val;
		scanf("%d", &val);
	}
	*dim = i;
	
	return;
}

void calcolaMinori (int seq[], int seq_ris[], int dim)
{
	int i, h, j;

	for (i = 0; i < dim; i++) {
		j = 0;
		for (h = 0; h < dim; h++) {
			if (seq[i] > seq[h]) {
				j++;
			}
		}
		seq_ris[i] = j;
	}

	return;
}
