#include <stdio.h>
#define NMAX 20

int main(int argc, char * argv[])
{
	int seq[NMAX];
	int num, i, dim, sentinella, tmp;

	do { /* Chiede all'utente la dimesione effettiva dell'array */
		scanf("%d", &num);
	} while (num <= 1);
	scanf("%d", &seq[0]);
	dim = 1;
	sentinella = 0;
	for (i = 1; i < num; i++) {
		scanf("%d", &tmp);
		if (!sentinella && tmp > seq[dim - 1]) {
			seq[dim] = tmp;
			dim++;
		} else {
			sentinella++;
		}
	}
	printf("\n");
	printf("%d\n", dim);
	for (i = 0; i < dim; i++) {
		printf("%d ", seq[i]);
	}
	printf("\n");
	return 0;
}
