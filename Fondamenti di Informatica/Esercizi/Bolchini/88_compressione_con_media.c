/* A partire da un array e da un intero k, mette al posto di ogni gruppo di k valori la loro media */
#include <stdio.h>
#include <stdlib.h>
#define NVAL 100

float * compressioneDati (float [], int, int, int *);

int main (int argc, char * argv[])
{
	int k, dim, dim_ris;
	float valori [NVAL];
	float *ris;
	
	for (k = 0; k < NVAL; k++) {
		scanf ("%f", &valori[k]);
	}

	do {
		scanf ("%d", &k);
	} while (k <= 0);

	ris = compressioneDati (valori, dim, k, &dim_ris);

	if (ris) {
		for (k = 0; k < dim_ris; k++) {
			printf ("%f\n", ris[k]);
		}
		free (ris);
	} else {
		printf ("compressioneDati: errore nell'allocare memoria per %d interi\n", dim_ris);
	}

	return 0;
}

float * compressioneDati (float valori[], int numValori, int k, int *numCompressi)
{
	int resto, *numCompressi, full, i, j;
	float sum;
	float *compressi;

	*numCompressi = numValori / k;
	if ((resto = numValori % k)) { /* tutta sta cosa serve a fargli approssimare per eccesso */
		*numCompressi++;
	}
	
	if (compressi = malloc (*numCompressi * sizeof (float))) {
		full = (numValori / k) * k; /* Se ho un numero divisibile per k i due k si semplificano, se no viene moltiplicata per k l'approssimazione per difetto di numValori / k, che è il numero di valori divisibili per k */
		for (i = 0; i < full; i += k) {
			sum = 0;
			for (j = 0; j < k; j++) {
				sum += valori [i + j];
			}
			compressi [i / k] = sum / k;
		}

		if (resto) {
			sum = 0;
			for (i = 0; i < resto; i++) { /* Somma tutti i valori restanti che non arrivano a fare un gruppo di k */
				sum += valori [full + i];
			}
			compressi [full /* Quà varrà i + 1*// k] = sum / resto;
		}
	}

	return compressi;
}