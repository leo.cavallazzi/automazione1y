#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define OPZIONI 2
#define BASE_SOURCE 10
#define BASE_TRG 2
#define FIRST '0'

char * int2bin (int);

int main (int argc, char * argv[])
{
	char *nomeExe, *bin;
	int val;
	
	nomeExe = argv[0];
	if (argc == OPZIONI) {
		val = atoi (argv[1]);
		bin = int2bin (val);
		printf ("%d_%d === %s_%d\n", val, BASE_SOURCE, bin, BASE_TRG);
		free (bin);
	} else if (argc < OPZIONI) {
		printf ("%s: Error: too few arguments\n", nomeExe);
	} else {
		printf ("%s: Error: too many arguments\n", nomeExe);
	}

	return 0;
}

char * int2bin (int val)
{
	char *bin;
	int dim, i, resto;

	dim = log (val) / log (BASE_TRG) + 1;
	if ((bin = malloc ((dim + 1) * sizeof (char)))) {
		for (i = (dim - 1); val > 0; i--) {
			bin[i] = val % BASE_TRG + FIRST;
			val = val / BASE_TRG;
		}
		bin[dim] = '\0';
	} else {
		printf ("Errore allocazione memoria per %d cifre\n", dim);
	}

	return bin;
}
