/*Acquisire un valore intero "fine", seguito da una sequenza di valori interi di lunghezza ignota e che si ritiene terminata quando l'utente inserisce il valore fine*/
/*Il valore fine non fa parte della sequenza. Il programma calcola e visualizza valor minimo, valor massimo e media dei valori acquisiti. Diamo per scontato che l'utente non inserisce subito il valore fine*/
#include <stdio.h>

int main(int argc, char * argv[])
{
	int fine, val, min, max, tot, count;
	float avg;
	scanf("%d", &fine);
	scanf("%d", &val);
	min = val;
	max = val;
	count = 1; /*se non inizializzate le variabili vengono inizializzate a 0*/
	tot = val;
	scanf("%d", &val); /*serve metterlo di nuovo qua per evitare che la prima volta si faccia due domande inutili, cioè se val è maggiore di max o minore di min, così comincia il ciclo con un nuovo val*/
	while (val != fine) {
		if (val > max) {
			max = val;
		} else if (val < min) {
			min = val;
		}
		tot = tot + val; /*tot += val;*/
		count++; /*count = count + 1;*/ /*invece ++count è la stessa cosa solo in questo caso quindi non farlo. https://qastack.it/programming/24853/c-what-is-the-difference-between-i-and-i */
		scanf("%d", &val);
	}
	avg = (float) tot / count;
	printf("%d %d %f\n", min, max, avg);
	return 0;
}
