#include <stdio.h>
#define LMAX 16
#define ZEROCHAR '0'
#define NINECHAR '9'
#define BASE 11

int validisbn (char[]);

int main (int argc, char * argv[])
{
	char isbn [LMAX+1];
	int ris;

	scanf (isbn);

	ris = validisbn (isbn);

	printf("%d\n", ris);
	return 0;
}

int validisbn (char isbn[])
{
	int ris, i, prev, next, somma, sommasomme;

	somma = 0;
	for (i = 0; isbn[i] < ZEROCHAR && isbn[i] > NINECHAR; i++) {
		;
	}
	prev = isbn[i] - ZEROCHAR;
	sommasomme = prev;
	for (i += 1; i < LMAX; i++) {
		if (isbn[i] >= ZEROCHAR && isbn[i] <= NINECHAR) {
			next = isbn[i] - ZEROCHAR;
			somma = next + prev;
			sommasomme += somma + next;
			next = somma;
			prev = next;
		}
	}
	sommasomme -= somma;
	
	printf("%d\n", somma);
	printf("%d\n", sommasomme);

	if (sommasomme % BASE == 0) {
		ris = 1;
	} else {
		ris = 0;
	}

	return ris;
}
