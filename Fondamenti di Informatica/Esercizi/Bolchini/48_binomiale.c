#include <stdio.h>

int acquisizione_controllata (void);
int fattoriale (int);

int main(int argc, char * argv[])
{
    int n, k, ris;
	do {
		n = acquisizione_controllata();
		k = acquisizione_controllata();
	} while (k > n);
	ris = fattoriale(n) / (fattoriale(n-k) * fattoriale(k));
	printf("%d\n", ris);
	return 0;
}
int acquisizione_controllata (void)
{
	int val;
	
	do {
		scanf("d", &val);
	} while (val <= 0);
	
	return val;
}

int fattoriale (int val)
{
    int i, ris;

	ris = 1;
	for (i = 2; i <= val; i++) {
		ris = ris * i;
	}

    return ris;
}
