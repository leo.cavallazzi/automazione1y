#include <stdio.h>
#define DIM 10
#define DIM_LIMITE 6
#define ALBERI_LEGALI_PICCOLA 1
#define ALBERI_LEGALI_GRANDE 2
#define FINPUT "./filematrix.txt"

typedef struct elemento_s {
    int haAlbero;
    char colore;
} elemento_t;

int checkGriglia (int, elemento_t [][DIM]);

int checkRiga (int, elemento_t [][DIM]);

int checkColonna (int, elemento_t [][DIM]);

int checkColore (int, elemento_t [][DIM]);

int checkDistanza (int, elemento_t [][DIM]);

int getColori(char [], elemento_t [][DIM], int);

int main (int argc, char * argv[])
{
    int i, j, ris, dim;
    int c1, c2; /* Coordinate */
    char holdS[DIM + 1]; /* Stringa della prima riga */
    elemento_t mat[DIM][DIM];
    FILE * fin;

    ris = 0;

    if ((fin = fopen (FINPUT, "a+"))) {
        fscanf (fin, "%d", &dim);
        for (i = 0; i < dim; i++) {
            fscanf (fin, "%s", holdS);
            for (j = 0; j < dim; j++) {
                mat[i][j].colore = holdS[j];
                mat[i][j].haAlbero = 0;
            }
        }
        
        while (!feof(fin)) {
            fscanf (fin, "%d", &c1);
            fscanf (fin, "%d", &c2);
            mat[c1][c2].haAlbero = 1;
        }

        ris = checkGriglia (dim, mat);

        fprintf (fin, "%d\n", ris);
        if (ris) {
            printf ("Hai vinto!\n");
        } else {
            printf ("Hai perso\n");
        }

        fclose (fin);
    } else {
        printf ("Errore nell'apertura del file\n");
    }
    
    return 0;
    
}
int checkGriglia (int dim, elemento_t mat[][DIM])
{
    int ris;
    int ris1, ris2, ris3, ris4;

    ris1 = checkRiga(dim, mat);
    ris2 = checkColonna(dim, mat);
    ris3 = checkColore(dim, mat);
    ris4 = checkDistanza(dim, mat);

    if (ris1) {
        printf ("Riga ok\n");
    } else {
        printf ("Errore riga\n");
    }
    if (ris2) {
        printf ("Colonna ok\n");
    } else {
        printf ("Errore colonna\n");
    }
    if (ris3) {
        printf ("Colore ok\n");
    } else {
        printf ("Errore colore\n");
    }
    if (ris4) {
        printf ("Distanza ok\n");
    } else {
        printf ("Errore distanza\n");
    }
    printf ("\n");

    if (ris1 && ris2 && ris3 && ris4) {
        ris = 1;
    } else {
        ris = 0;
    }

    return ris;
}

int checkRiga (int dim, elemento_t mat [][DIM])
{
    int i, j, count, vabene, num;

    if (dim > DIM_LIMITE) {
        num = ALBERI_LEGALI_GRANDE;
    } else {
        num = ALBERI_LEGALI_PICCOLA;
    }
    vabene = 1;
    for (i = 0; i < dim && vabene; i++) {
        count = 0;
        for (j = 0; j < dim && count <= num; j++){
            if (mat[i][j].haAlbero == 1) {
                count++;
            }
        }
        if (count != num) {
            vabene = 0;
        }
    }
    return vabene;
}
int checkColonna (int dim, elemento_t mat [][DIM])
{
    int i, j, count, vabene, num;

    if (dim > DIM_LIMITE) {
        num = ALBERI_LEGALI_GRANDE;
    } else {
        num = ALBERI_LEGALI_PICCOLA;
    }
    vabene = 1;
    for (i = 0; i < dim && vabene; i++) {
        count = 0;
        for (j = 0; j < dim && count <= num; j++){
            if (mat[j][i].haAlbero == 1) {
                count++;
            }
        }
        if (count != num) {
            vabene = 0;
        }
    }
    return vabene;
}


int checkDistanza (int dim, elemento_t mat[][DIM]){
    int i, j, vabene, k, l;

    vabene = 1;

    for (i = 0; i < dim && vabene; i++) {
        for (j = 0; j < dim && vabene; j++) {
            if (mat[i][j].haAlbero == 1) {
                for (k = i - 1; k <= i + 1 && k >= 0 && k < dim && vabene; k++) {
                    for (l = j - 1; l <= j + 1 && l >= 0 && l < dim && vabene; l++) {
                        if (mat[k][l].haAlbero == 1 && (k != i || l != j)) {
                            vabene = 0;
                        }
                    }
                }
            }
        }
        
    }
    return vabene;
}

int checkColore (int dim, elemento_t mat[][DIM])
{
    char colori[DIM];
    int i, j, k, flag, numColori, count, num;

    if (dim > DIM_LIMITE) {
        num = ALBERI_LEGALI_GRANDE;
    } else {
        num = ALBERI_LEGALI_PICCOLA;
    }

    numColori = getColori (colori, mat, dim);

    flag = 1;

    for (k = 0; k < numColori && flag; k++) {
        count = 0;
        for (i = 0; i < dim; i++) {
            for (j = 0; j < dim; j++) {
                if (mat[i][j].colore == colori[k] && mat[i][j].haAlbero == 1) {
                    count++;
                }
            }
        }
        if (count != num) {
            flag = 0;
        }
    }

    return flag;
}

int getColori (char colori[], elemento_t mat[][DIM], int dim)
{
    int i, j, k, check, numColori; /* check indica se il colore corrente è già stato trovato */
    char holdC;

    numColori = 0;

    for (i = 0; i < dim; i++) {
        for (j = 0; j < dim; j++) {
            holdC = mat[i][j].colore;
            check = 0;
            for (k = 0; k < numColori && !check; k++) {
                if (colori[k] == holdC) {
                    check = 1;
                }
            }
            if (!check) {
                colori[numColori] = holdC;
                numColori++;
            }
        }
    }

    return numColori;
}