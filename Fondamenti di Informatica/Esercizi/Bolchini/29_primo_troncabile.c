/* primi troncabili i numeri primi che rimangono tali se vengono tolte le cifre a una a una da destra */
#include <stdio.h>
#define BASE 10

int main(int argc, char * argv[])
{
	int num, primo, tmp, cifre, div;
	do {
		scanf("%d", &num);
	} while (num <= 0);
	tmp = num;
	primo = 1;
	for (cifre = 0; tmp > 0 && primo; cifre++) {
		for (div = 2; div <= (tmp / 2) && primo; div++) {
			if (tmp % div == 0) {
				primo = 0;
			}
		}
		tmp = tmp / BASE;
	}
	printf("%d\n", primo);
	return 0;
}