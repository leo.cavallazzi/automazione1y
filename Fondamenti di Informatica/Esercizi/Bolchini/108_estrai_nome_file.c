#include <stdio.h>
#define SEP '/'
#define CMAX 255

char * trovaNomeFile (char [], char);

int main (int argc, char * argv[])
{
	char percorso[CMAX+1];
	char *ris;

	scanf ("%s", percorso);
	ris = trovaNomeFile (percorso, SEP);
	
	printf ("%s\n", ris);
	return 0;
}

char * trovaNomeFile (char percorso[], char sep)
{
	int i, ultimoSep;

	ultimoSep = 0;
	for (i = 0; percorso[i] != '\0'; i++) {
		if (percorso[i] == sep) {
			ultimoSep = i;
		}
	}
	if (ultimoSep) {
		ultimoSep++; /* Così mostra a partire dal carattere dopo lo slash */
	}

	return &percorso[ultimoSep];
}
