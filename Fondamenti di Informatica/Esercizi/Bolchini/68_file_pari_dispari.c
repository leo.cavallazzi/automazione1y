/* Legge 100 valori interi da un file (che di base vuol dire testo ASCII) ./dati.txt (cioè nella stessa cartella del source */
/* Scrive su pari.txt i pari e su dispari.txt i dispari */
/* SO già che almeno 100 valori ci sono */
#include <stdio.h>
#define INPUT "./dati.txt"
#define FILEP "./pari.txt"
#define FILED "./dispari.txt"
#define NVAL 100

int main(int argc, char * argv[])
{
	FILE *fin, *foutp, *foutd;
	int i, num;

	if (fin = fopen (INPUT, "r")) { /* è sbagliato mettere gli && perché poi mi trovo a chiudere cose che non ho mai aperto se qualcosa fallisce */
		if (foutp = fopen (FILEP, "w")) {
			if (foutd = fopen (FILED, "w")) {
				for (i = 0; i < NVAL; i++) {
					fscanf (fin, "%d", &num); /* è capace da solo di prendere un valore diverso a ogni giro */
					if (num % 2) {
						fprintf (foutd, "%d\n", num);
					} else {
						fprintf (foutp, "%d\n", num);
					}
				}
				fclose (foutd);
			} else {
				printf ("Problemi con il file %s\n", FILED);
			}
			fclose (foutp);
		} else {
			printf ("Problemi con il file %s\n", FILEP);
		}
		fclose (fin);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}