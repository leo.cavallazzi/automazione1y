#include <stdio.h>
#include <string.h>
#define INPUT "./dizionario.txt"
#define CMAX 25

int main (int argc, char * argv[])
{
	char parola [CMAX+1], curS [CMAX+1];
	int j, dim, tmp;
	FILE *fin;

	scanf ("%s", parola);

	if ((fin = fopen (INPUT, "r"))) {
		dim = strlen (parola);
		while (fscanf (fin, "%s", curS) != EOF) {
			if (strlen (curS) == dim) {
				tmp = 0;
				for (j = 0; j < dim; j++) {
					if (curS [j] != parola [j]) {
						tmp++;
					}
				}
				if (tmp == 1) { /* è autorizzata ad avere una sola lettera di scarto, ovunque sia */
					printf ("%s\n", curS);
				}
			}
		}
		fclose (fin);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}
