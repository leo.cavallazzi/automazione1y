#include <stdio.h>
#define INPUT "./dati.txt"
#define LOG "./query.log"
#define CMAX 50

comparaStringhe (char[], char[]);

int main(int argc, char * argv[])
{
	FILE *fin, *log;
	char parola[CMAX+1], tmp[CMAX+1];
	int ris;

	if (fin = fopen (INPUT, "r")) {
		if (log = fopen (LOG, "a")) {
			scanf ("%s", parola);
			ris = 0;
			while (fscanf (fin, "%s", tmp) != EOF /* Non si può mettere fuori e poi mettere tmp diverso da. Per metterla fuori dentro devi usare !feof(fin) */&& ris == 0) {
				ris = comparaStringhe(parola, tmp);
			}
			printf ("%d\n", ris);
			fprintf (log, "%s %d\n", parola, ris);
			fclose (log);
		} else {
			printf ("Problemi con il file %s\n", LOG);
		}
		fclose (fin);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}

int comparaStringhe (char s1[], char s2[]) /* come strcmp */
{
	int i, uguali;

	uguali = 1;
	for (i = 0; s1[i] != "\0" || s2[i] != "\0" && uguali == 1; i++) {
		if (s1[i] != s2[i]) {
			uguali = 0;
		}
	}
	
	return uguali;
}