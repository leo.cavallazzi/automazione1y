#include <stdio.h>
#include <stdlib.h>
#define CMAX 50

char * interseca (char [], char []);
void rimuoviDuplicati (char []);

int main (int argc, char * argv[])
{
	char s1[CMAX+1], s2[CMAX+1];
	char *intersezione;

	scanf ("%s%s", s1, s2);
	
	intersezione = interseca (s1, s2);

	if (intersezione) {
		printf ("%s\n", intersezione);
		free (intersezione);
	} else {
		printf ("Interseca: errore nell'allocare memoria\n");
	}

	return 0;
}

char * interseca (char s1[], char s2[])
{
	char *intersezione;
	int dim, i, j;

	rimuoviDuplicati (s1);
	rimuoviDuplicati (s2);

	dim = 0;
	for (i = 0; s1[i] != '\0'; i++) {
		for (j = 0; s2[j] != '\0'; j++) {
			if (s1[i] == s2[j]) {
				dim++;
			}
		}
	}

	if ((intersezione = malloc ((dim + 1) * sizeof (char)))) {
		dim = 0;
		for (i = 0; s1[i] != '\0'; i++) {
			for (j = 0; s2[j] != '\0'; j++) {
				if (s1[i] == s2[j]) {
					intersezione [dim] = s1 [i];
					dim++;
				}
			}
		}
		intersezione [dim] = '\0';
	}

	return intersezione;
}

void rimuoviDuplicati (char seq[])
{
	int trovato, i, j, dim;

	dim = 0;
	for (i = 0; seq[i] != '\0'; i++) {
		trovato = 0;
		for (j = 0; j < i && !trovato; j++) {
			if (seq[i] == seq[j]) {
				trovato++;
			}
		}
		if (!trovato) {
			seq[dim] = seq[i];
			dim++;
		}
	}

	seq[dim] = '\0';

	return;
}