/* Fa un altro array a partire da 1 2 3 4 esce 1 3 6 10, cioè ogni valore è la somma di sé stesso e tutti quelli prima */
#include <stdio.h>
#define NMAX 100
#define STOP 0 /* si ferma se è uguale a zero */

void operazione (float [], int*);
void acquisizione_controllata (float [], int*);

int main(int argc, char * argv[])
{
	int seq[NMAX], ris[NMAX], dim, i;

	acquisizione_controllata(seq, &dim);

	operazione(seq, ris, dim);

	for (i = 0; i < dim; i++) {
		printf("%f ", ris[i]);
	}
	return 0;
}

void operazione (float seq[], float ris[], int dim);
{
	int i;
	
	ris[0] = seq[0];
	for (i = 1; i < dim; i++) {
		ris[i] = ris[i-1] + seq[i];
	}

	return;
}

void acquisizione_controllata (float seq[], int*dim)
{
	int i;
	float val;
	
	scanf("%f", &val);
	for (i = 0; i < NMAX && val != STOP; i++) {
		seq[i] = val;
		scanf("%f", &val);
	}
	*dim = i;
	
	return;
}
