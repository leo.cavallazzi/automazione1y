#include <stdio.h>
#define DIM 25

int main (int argc, char * argv[])
{
	int arr[DIM];
	int i, j, k, max, prod;

	for (i = 0; i < DIM; i++) {
		scanf ("%d", &arr[i]);
	}
	
	max = 0;
	for (i = 0; i < DIM; i++) {
		for (j = (i + 1); j < DIM; j++) {
			for (k = (j + 1); k < DIM; k++) {
				prod = arr[i]*arr[j]*arr[k];
				if (prod > max) {
					max = prod;
				}
			}
		}
	}

	printf ("%d\n", max);
	return 0;
}