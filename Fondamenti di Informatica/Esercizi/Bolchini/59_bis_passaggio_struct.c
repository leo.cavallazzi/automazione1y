/* Passaggio al sottoprogramma di struct */
/* Se mai dovessi scrivere tipo scanf &*dato potrei scrivere scanf dato */
/* Come scritto sotto, mai usare le define del main nel sottoprogramma anche se si può, posso fare delle define del sottoprogramma e però le devo mettere sopra insieme a quelle del main */
#include <stdio.h>
#define MAX 100

typedef struct date_s {
	int giorno, mese, anno;
} date_t;

void insieme (date_t [], date_t*, int*, int*, int);
void acquisisci_input (date_t [], int);

int main(int argc, char * argv[])
{
	date_t input[MAX], trg;
	int dim, compleanno_insieme, nati_insieme;

	printf("Quante date vuoi mettere?\n");
	do {
		scanf("%d", &dim);
	} while (dim <= 0);

	printf("Ora le date\n");
	acquisisci_input (input, dim);

	printf("Ora la data da controllare\n");
	do {
		scanf("%d", &trg.giorno);
	} while (trg.giorno <= 0);
	do {
		scanf("%d", &trg.mese);
	} while (trg.mese <= 0);
	scanf("%d", &trg.anno);
	insieme(input, &trg, &compleanno_insieme, &nati_insieme, dim);

	printf("%d %d\n", compleanno_insieme, nati_insieme);
	return 0;
}

void insieme (date_t input[], date_t*trg /* Le variabili strutturate è sempre meglio passarle per indirizzo */, int*compleanno_insieme, int*nati_insieme, int dim)
{
	int i, comple, nati;
	comple = 0;
	nati = 0;

	for (i = 0; i < dim; i++) {
		if (input[i].giorno == trg->giorno /* Come scrivere (*trg).giorno */ && input[i].mese == trg->mese) {
			comple++;
			if (input[i].anno == trg->anno) {
				nati++;
			}
		}
	}
	*compleanno_insieme = comple;
	*nati_insieme = nati;

	return /* Per ipotesi posso returnare più cose: se scrivessi return trg manderebbe indietro 3 valori, ma è meglio non farlo */;
}

void acquisisci_input (date_t input[], int dim)
{
	int i;
	for (i = 0; i < dim; i++) { /* In generale, mai mettere quei delle define che ti immagini ci siano nel main, solo quelle che ti sei messo tu che hai scritto il sottoprogramma */
		do {
			scanf("%d", &input[i].giorno);
		} while (input[i].giorno <= 0);
		do {
			scanf("%d", &input[i].mese);
		} while (input[i].mese <= 0);
		scanf("%d", &input[i].anno);
	}
	
	return;
}
