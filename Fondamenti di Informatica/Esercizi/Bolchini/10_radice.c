/*Acquisire un valore intero positivo e finchè non è tale lo richiede, quindi calcola e visualizza la parte intera della radice quadrata*/
#include <stdio.h>

int main(int argc, char * argv[])
{
	int val, count, ris;
	do {
		scanf("%d", &val);
	} while (val <= 0);
	count = 1;
	while (count * count <= val) { /* se le condizioni si complicano è meglio fare una variabile in più */
		count++; /* prova con 1x1 poi con 2x2 e così via */
	}
	ris = count - 1; /* toglie 1 perché il while arriva al valore subito dopo, altrimenti verrebbe approssimato per eccesso */
	printf("%d\n", ris);
	return 0;
}
