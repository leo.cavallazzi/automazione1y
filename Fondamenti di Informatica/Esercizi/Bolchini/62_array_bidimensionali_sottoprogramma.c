/* Passaggio di array binimensionali. Sottoprogramma che calcola e trasmette al chiamante l'indice della riga la cui somma dei valori è minima e poi quello della colonna */
/* In tra perché nel testo dell'esercizio all'esame ci sarà scritto come si chiama la define delle colonne */
#include <stdio.h>
#define RIGHE 3
#define COLONNE 4

void indici (int [][COLONNE], int*, int*, int, int);

int main(int argc, char * argv[])
{
	int mat[RIGHE][COLONNE], i, j, nr, nc, ir, ic;

	for (i = 0; i < RIGHE; i++) {
		for (j = 0; j < COLONNE; j++) {
			scanf ("%d", &mat[i][j]);
		}
	}
	nr = RIGHE;
	nc = COLONNE;
	
	indici (mat, &ir, &ic, nr, nc);

	printf ("%d;%d", ir, ic);
	return 0;
}

void indici (int mat[][COLONNE]/* Bisogna specificare tutte le dimensioni tranne la prima */, int*ir, int*ic, int nr, int nc /* Questi sono come le define RIGHE e COLONNE che non uso perché non sono io a fare il main e non so neanche se tutte le colonne dichiarate sono state usate quindi chiedo che mi vengano passati */)
/* void indici (int*mat Potevo scrivere così e funzionava e non avevo il vincolo di dimensione, però perdo leggibilità e sopratutto cambiava sotto, int*ir, int*ic, int nr, int nc) */
{
	int i, j, indexr, indexc, sumr, sumc, minr, minc;

	minr = 0;
	for (j = 0; j < nc; j++) {
		minr += mat[0][j]; /* Somma tutti i valori sulla prima riga */
	}
	indexr = 0;
	for (i = 1; i < nr; i++) {
		sumr = 0;
		for (j = 0; j < nc; j++) {
			sumr += mat[i][j]; /* Al posto di mat[i][j], se avessi scelto la seconda impostazione sopra, avre messo *(mat + i*nc + j), però in questo caso nc deve proprio essere il massimo numero di colonne, cioè la define COLONNE, però resta più flessibile perché potrei definire un altro parametro di ingresso che si chiama nmaxc e il sottoprogramma diventa riutilizzabile */
		}
		if (sumr <= minr) {
			minr = sumr;
			indexr = i;
		}
	}

	minc = 0;
	for (j = 0; j < nr; j++) {
		minc += mat[j][0]; /* Somma tutti i valori sulla prima colonna */
	}
	indexc = 0;
	for (i = 1; i < nc; i++) {
		sumc = 0;
		for (j = 0; j < nr; j++) {
			sumc += mat[j][i];
		}
		if (sumc <= minc) {
			minc = sumc;
			indexc = i;
		}
	}

	*ir = indexr + 1; /* Solo perché così all'utente si mostra la colonna zero come uno */
	*ic = indexc + 1;

	return;
}