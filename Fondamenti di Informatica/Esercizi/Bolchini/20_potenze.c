/* Fa x^y */
#include <stdio.h>

int main(int argc, char * argv[])
{
    int x, y, i;
	float ris;
	scanf("%d", &x);
	scanf("%d", &y);
	if (y < 0) {
		y = -y;
		ris = 1;
		for (i = 0; i < y; i++) {
			ris = ris * x;
		}
		ris = 1 / ris;
	} else {
		ris = 1; /* Elemento neutro e in più è il risultato di qualsiasi numero alla 0 quindi se non entro nel ciclo stampa giustamente 1 */
		for (i = 0; i < y; i++) {
			ris = ris * x;
		}
	}
	printf("%f\n", ris);
	
    return 0;
}