#include <stdio.h>
#include <stdlib.h>
#define MAX 50
#define OPTIONS 3

char * rep (char [], int);

int main (int argc, char * argv[])
{
	char * s_ris;

	if (argc == OPTIONS) {
		s_ris = rep (argv[1], atoi (argv[2]));
		printf ("%s\n", s_ris);
	} else if (argc < OPTIONS) {
		printf ("Too few arguments\n");
	} else {
		printf ("Too many arguments\n");
	}

	return 0;
}

char * rep (char s[], int n)
{
	int dim, i, j, size;
	char * s_ris;

	for (dim = 0; s[dim] != '\0'; dim++) {
		;
	}

	size = n * dim;
	if ((s_ris = malloc ((size + 1) * sizeof (char)))) {
		i = 0;
		while (i < size) {
			for (j = 0; s[j] != '\0'; j++) {
				s_ris[i] = s[j];
				i++;
			}
		}
		s_ris[size] = '\0';
	} else {
		printf ("Errore nell'allocazione memoria per la stringa risultante\n");
	}

	return s_ris;
}