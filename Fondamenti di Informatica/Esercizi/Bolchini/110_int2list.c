#include "list_int.h"
#define BASE 10

lista_t * int2list (int);

int main (int argc, char * argv[])
{
	lista_t * head;
	int val;
	
	scanf ("%d", &val);
	head = int2list (val);

	printl (head);
	head = empty (head);
	return 0;
}

lista_t * int2list (int n)
{
	lista_t *head = NULL;
	int i, valore;

	if (n < 0) {
		n = -n;
		while (n > 0) {
			valore = n % BASE;
			for (i = 0; i < valore; i++) {
				head = append (head, valore);
			}
			n = n / BASE;
		}
	} else {
		while (n > 0) {
			valore = n % BASE;
			for (i = 0; i < valore; i++) {
				head = push (head, valore);
			}
			n = n / BASE;
		}
	}

	return head;
}
