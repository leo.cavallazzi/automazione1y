#include <stdio.h>
#include <stdlib.h>
#include "list_char.h"
#define TONDAO '('
#define TONDAC ')'
#define QUADO '['
#define QUADC ']'
#define GRAFO '{'
#define GRAFC '}'
#define LMAX 100

int isAnnidata (lista_t *);
int valid (lista_t*);

int main(int argc, char * argv[])
{
	char seqpar[LMAX + 1];
	lista_t * h = NULL;
	int i, ris, rus;
	
	scanf ("%s", seqpar);
	for (i = 0; seqpar[i] != '\0'; i++) {
		h = append(h, seqpar[i]);
	}
	if ((rus = valid(h))) {
		ris = isAnnidata(h);
	} else {
		ris = 0;
	}

	printf("%d\n", ris);
	/*h = emptylist(h);*/
	return 0;
}
int isAnnidata(lista_t * h)
{
	lista_t *tmp, *ptr;
	static int ris = 1;
	int pos;
	
	tmp = h;
	if (tmp != NULL && tmp->next != NULL && ris == 1) {
		if (tmp && tmp->val == TONDAO) {
			if (locc(tmp, TONDAC)) {
				ptr = find (tmp, TONDAC);
				pos = ptr->val;
				tmp = delPos (tmp, pos, SEEK_SET);
				tmp = delPos (tmp, 0, SEEK_SET);
				if (tmp) {
					ris = isAnnidata(tmp);
				}
			} else {
				ris = 0;
			}
		}
		if (tmp && tmp->val == QUADO) {
			if (locc(tmp->next, QUADC)) {
				ptr = find (tmp, QUADC);
				pos = ptr->val;
				tmp = delPos (tmp, pos, SEEK_SET);
				tmp = delPos (tmp, 0, SEEK_SET);
				if (tmp) {
					ris = isAnnidata(tmp);
				}
			} else {
				ris = 0;
			}
		}
		if (tmp && tmp->val == GRAFO) {
			if (locc(tmp, GRAFC)) {
				ptr = find (tmp, GRAFC);
				pos = ptr->val;
				tmp = delPos (tmp, pos, SEEK_SET);
				tmp = delPos (tmp, 0, SEEK_SET);
				if (tmp) {
					ris = isAnnidata(tmp);
				}
			} else {
				ris = 0;
			}
		}
	} else {
		ris = 0;
	}

	return ris;
}
int valid(lista_t *h)
{
	int ris;
	lista_t *tmp;
	
	tmp = h;
	ris = 1;
	while (tmp != NULL && tmp->next != NULL && ris == 1) {
		if (tmp->val == TONDAO && (tmp->next->val == QUADC || tmp->next->val == GRAFC)) {
			ris = 0;
		}
		else if (tmp->val == QUADO && (tmp->next->val == TONDAC || tmp->next->val == GRAFC)) {
			ris = 0;
		}
		else if (tmp->val == GRAFO && (tmp->next->val == QUADC || tmp->next->val == TONDAC)) {
			ris = 0;
		} else {
			tmp = tmp->next;
		}
	}
	return ris;
}