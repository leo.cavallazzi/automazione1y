#include <stdio.h>
#define DIM 5
#define LIM_DOWN 1
#define LIM_UP 5

int proprieta (int [][DIM]);
int isavailable (int, int []);

int main (int argc, char * argv[])
{
	int i, j, ris;
	int mat [DIM][DIM];

	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			scanf ("%d", &mat[i][j]);
		}
	}

	ris = proprieta (mat);
	printf ("%d\n", ris);

	return 0;
}

int proprieta (int mat[][DIM])
{
	int ris, i, j, k;
	int available[DIM];

	ris = 1;
	for (k = 0; k < DIM; k++) {
		available[k] = (k + 1);
	}
	for (i = 0; i < DIM && ris; i++) {
		for (j = 0; j < DIM && ris; j++) {
			if (mat[i][j] < LIM_DOWN || mat[i][j] > LIM_UP || !isavailable(mat[i][j], available)) {
				ris = 0;
			} else {
				available[mat[i][j]-1] = 0;
			}
		}
		for (k = 0; k < DIM && ris; k++) {
			available[k] = (k + 1);
		}
	}

	for (j = 0; j < DIM && ris; j++) {
		for (i = 0; i < DIM && ris; i++) {
			if (mat[i][j] < LIM_DOWN || mat[i][j] > LIM_UP || !isavailable(mat[i][j], available)) {
				ris = 0;
			} else {
				available[mat[i][j]-1] = 0;
			}
		}
		for (k = 0; k < DIM && ris; k++) {
			available[k] = (k + 1);
		}
	}

	return ris;
}

int isavailable (int val, int available[])
{
	int i, ris;
	
	ris = 0;
	for (i = 0; i < DIM; i++) {
		if (val == available[i]) {
			ris = 1;
		}
	}

	return ris;
}