#include <stdio.h>
#define NMAX 50
#define STOP 0

int main(int argc, char * argv[])
{
    int val[NMAX], i;
    int num, dim; 
    int senzaprimi, eprimo, divisore, metavalore;
    
    dim = 0;
    scanf("%d", &num);
    while(num > STOP) {
        val[dim] = num;
        dim++;
        scanf("%d", &num);
    }
    senzaprimi = 1;
    for (i = 0; i < dim && senzaprimi == 1; i++) {
        eprimo = 1; /* il valore che sto per analizzare ipotizzo sia primo */
        if (val[i] == 1 || val[i] % 2 == 0 && val[i] != 2) {  /* 1 e pari diversi da 2 non sono primi */
            eprimo = 0;
        } else { /* controllo tutti gli altri, 2 incluso */
            metavalore = val[i] / 2; /* oppure divisore*divisore <= val[i] && eprimo. Questa regola non trova tutti i divisori ma se il numero non è primo sicuramente trovi un divisore con questa regola */
            for (divisore = 3; divisore <= metavalore && eprimo == 1; divisore = divisore + 2) {
                if (val[i] % divisore == 0) {
                    eprimo = 0;
                }
            }
        }
        if (eprimo) {
            senzaprimi = 0;
        }
    }
    printf("%d\n", senzaprimi);
    return 0;
}
